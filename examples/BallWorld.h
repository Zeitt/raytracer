#ifndef BALL_WORLD_H
#define BALL_WORLD_H

#include <raytracer/Ray.hpp>
#include <raytracer/Sphere.hpp>
#include <raytracer/World.hpp>
#include <raytracer/Utils.hpp>
#include <raytracer/View.hpp>
#include <raytracer/Materials.hpp>
#include <raytracer/Box.hpp>

/**
 * @brief World full of Balls. Used mainly to test performance.
 */
struct BallWorld
{
	/**
	 * @brief Constructs world full of balls.
	 * @param maxBalls Maximum number of balls to place.
	 * @param maxDistance Maximum range from origin where to place balls.
	 */
	explicit BallWorld(size_t maxBalls, size_t maxDistance);
	/**
	 * @brief Destructor.
	 */
	~BallWorld();

	raytracer::World world;
	std::vector<raytracer::Texture*> textures;
	std::vector<raytracer::Material*> materials;
};

BallWorld::BallWorld(size_t maxBalls, size_t maxDistance):
	world(0.01f, 100.f)
{
	float R = cosf(M_PI/4.f);
	raytracer::Randomizer rand;

	std::vector<glm::vec4> positions;
	auto isOccupied = [&positions](glm::vec3 p, float size)->bool
	{
		bool occupied{false};
		for(auto& pos : positions)
		{
			float distance = glm::distance(glm::vec3{pos}, p);
			bool temp = fabsf(distance) < (size + pos.w + 0.3f);
			occupied = std::max(temp,occupied);
		}
		return occupied;
	};

	for(size_t st = 0; st < maxBalls; ++st)
	{
		float size = R/2.f + rand.randomFloat();
		glm::vec3 pos{};
		size_t maxTries = 100;
		while(isOccupied(pos,size) && maxTries-- > 1)
		{
			pos = rand.randomInUnitSphere();
			pos.x *= maxDistance/2.f;
			pos.x -= float(maxDistance) / 4.f;
			pos.z *= maxDistance/2.f;
			pos.z -= float(maxDistance) / 4.f;
			pos.y = rand.randomFloat();
		}
		if(maxTries < 1)
		{
			continue;
		}

		glm::vec3 color = (glm::vec3{0.1f} + (rand.randomInUnitSphere()*2.f))/2.f;
		raytracer::Material* matPtr{nullptr};
		size_t dice = lroundf(rand.randomFloat()*10.f);
		if(dice < 2)
		{
			auto colorPtr = new raytracer::Color(color);
			textures.push_back(colorPtr);
			matPtr = new raytracer::Metal(colorPtr,rand.randomFloat());
		}
		else if(dice > 9)
		{
			auto checker = new raytracer::CheckerTexture(rand.randomInUnitSphere(), rand.randomInUnitSphere());
			textures.push_back(checker);
			matPtr = new raytracer::Lambertian(checker);
		}
		else
		{
			auto colorPtr = new raytracer::Color(color);
			textures.push_back(colorPtr);
			matPtr = new raytracer::Lambertian(colorPtr);
		}
		materials.push_back(matPtr);
		world.addSphere(pos, size, matPtr);
		positions.emplace_back(pos.x,pos.y,pos.z,size);
	}
}
BallWorld::~BallWorld()
{
	for(raytracer::Material* material : materials)
	{
		delete material;
	}
	for(raytracer::Texture* texture : textures)
	{
		delete texture;
	}
}


#endif //BALL_WORLD_H