#ifndef CORNELLS_BOX_H
#define CORNELLS_BOX_H

#include <raytracer/Ray.hpp>
#include <raytracer/Sphere.hpp>
#include <raytracer/World.hpp>
#include <raytracer/Utils.hpp>
#include <raytracer/View.hpp>
#include <raytracer/Materials.hpp>
#include <raytracer/Box.hpp>

/**
 * @brief Cornell's Boxes
 */
struct CornellsBox
{
	/**
	 * @brief Constructor. Creates World with 5 walls, 2 boxes and single light.
	 */
	explicit CornellsBox();

	raytracer::World world;

	raytracer::LightMaterial lightMat;
	raytracer::Color green;
	raytracer::Lambertian greenMat;
	raytracer::Color red;
	raytracer::Lambertian redMat;
	raytracer::Color white;
	raytracer::Lambertian whiteMat;

	raytracer::PlaneXY back;
	raytracer::PlaneXZ top;
	raytracer::PlaneYZ left;

	raytracer::Box smallBox;
	raytracer::Box bigBox;
	raytracer::YRotated rotatedSmall;
	raytracer::YRotated rotatedBig;
};


CornellsBox::CornellsBox():
	world(0.001f, MAXFLOAT),
	lightMat(glm::vec3{15.f}),
	green(glm::vec3{0.1f,0.7f,0.1f}),
	greenMat(&green),
	red(glm::vec3{0.7f,0.1f,0.1f}),
	redMat(&red),
	white(glm::vec3{0.7f,0.7f,0.7f}),
	whiteMat(&white),
	back({}, glm::vec2{555, 555}, 555, &whiteMat),
	top({},glm::vec2{555,555}, 555, &whiteMat),
	left({},glm::vec2{555, 555}, 555, &greenMat),
	//smallBox(glm::vec3{130,0,65}, glm::vec3{295, 165, 230}, &whiteMat),
	smallBox({}, glm::vec3{165, 165, 165}, &whiteMat),
	bigBox({}, glm::vec3{165,330,165}, &whiteMat),
	rotatedSmall(smallBox, -18.f),
	rotatedBig(bigBox, 15.f)
{
	world.addFlipped(back);

	world.addPlaneXZ(glm::vec2{}, glm::vec2{555,555}, 0.f, &whiteMat);
	world.addFlipped(top);

	world.addPlaneXZ(glm::vec2{213,227}, glm::vec2{343,332}, 554, &lightMat);

	world.addPlaneYZ(glm::vec2{}, glm::vec2{555,555.f}, 0.f, &redMat);
	world.addFlipped(left);

	//world.addYRotated(&smallBox, -18.f);
	world.addTranslated(rotatedSmall,glm::vec3{130,0,65} );
	world.addTranslated(rotatedBig,glm::vec3{265,0,295} );
}

#endif //CORNELLS_BOX_H