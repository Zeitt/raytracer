#include <raytracer/Box.hpp>

namespace raytracer
{
	Box::Box(glm::vec3 begin, glm::vec3 end, Material* material):
			Object(material),
			mAABB({begin, end}),
			mBack(glm::vec2{mAABB.min.x, mAABB.min.y}, glm::vec2{mAABB.max.x, mAABB.max.y}, mAABB.max.z, material),
			mFront(glm::vec2{mAABB.min.x, mAABB.min.y}, glm::vec2{mAABB.max.x, mAABB.max.y}, mAABB.min.z,material),
			mTop(glm::vec2{mAABB.min.x, mAABB.min.z}, glm::vec2{mAABB.max.x, mAABB.max.z}, mAABB.max.y, material),
			mBottom(glm::vec2{mAABB.min.x, mAABB.min.z}, glm::vec2{mAABB.max.x, mAABB.max.z}, mAABB.min.y, material),
			mLeft(glm::vec2{mAABB.min.y, mAABB.min.z}, glm::vec2{mAABB.max.y, mAABB.max.z}, mAABB.max.x, material),
			mRight(glm::vec2{mAABB.min.y, mAABB.min.z}, glm::vec2{mAABB.max.y, mAABB.max.z}, mAABB.min.x, material),
			mBackFlipped(mBack),
			mBottomFlipped(mBottom),
			mRightFlipped(mRight)
	{
		mSides.push_back(&mBackFlipped);
		mSides.push_back(&mFront);
		mSides.push_back(&mTop);
		mSides.push_back(&mBottomFlipped);
		mSides.push_back(&mLeft);
		mSides.push_back(&mRightFlipped);
	}

	bool aabbBoxHitTest(AABB aabb, const Ray& ray, float minDistance, float maxDistance)
	{
		for(size_t st = 0; st < 3; ++st)
		{
			float t0 = std::min((aabb.min[st] - ray.getOrigin()[st]) / (ray.getDirection()[st]),
								(aabb.max[st] - ray.getOrigin()[st])/ (ray.getDirection()[st]));
			float t1 = std::max((aabb.min[st] - ray.getOrigin()[st]) / (ray.getDirection()[st]),
								(aabb.max[st] - ray.getOrigin()[st])/ (ray.getDirection()[st]));
			minDistance = std::max(t0, minDistance);
			maxDistance = std::min(t1, maxDistance);
			if(maxDistance <= minDistance)
			{
				return false;
			}
		}
		return true;
	}

	bool Box::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		bool hitAnything{false};
		if(aabbBoxHitTest(getAABB(),ray, minDistance, maxDistance))
		{
			float closest = maxDistance;
			HitRecord record{};
			for (auto &obj : mSides)
			{
				if (obj->hitTest(ray, minDistance, closest, record))
				{
					hitAnything = true;
					closest = record.distance;
					recordOut = record;
				}
			}
		}
		return  hitAnything;
	}
}