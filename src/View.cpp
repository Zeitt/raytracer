#include <raytracer/View.hpp>

#include <mutex>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>

namespace raytracer
{
	View::View(World* w, float fov, size_t width, size_t height, size_t sampleCount /*= 4*/, bool useThreading /*= true*/):
				mVFov(glm::radians(fov)),
				mWidth(width),
				mHeight(height),
				mThreadCount(std::max(int64_t(std::thread::hardware_concurrency()) - 2, 1L)),
				mWorld(w)
	{
		lookFromTo(mPos,{0.f,0.f,mDepth});

		if (sampleCount > 1)
		{
			for(size_t st = 0; st < sampleCount; ++st)
			{
				mSampleTable.emplace_back(mRand.randomInPixel());
			}
		}
		else
		{
			// Sample only from center of pixel
			mSampleTable.emplace_back(0.f, 0.f);
		}

		size_t sideSize = 16;
		size_t xTiles = ceilf(float(width)/float(sideSize));
		size_t yTiles = ceilf(float(height)/float(sideSize));

		mThreadCount =  useThreading ? std::min(mThreadCount, xTiles*yTiles) : 1;

		for(size_t y = 0; y < yTiles; ++y)
		{
			for(size_t x = 0; x < xTiles; ++x)
			{
				Tile tile;
				tile.begin = {x*sideSize, y*sideSize};
				tile.end = {(x+1)*sideSize, (y+1)*sideSize};
				tile.end.x = std::min(width, tile.end.x);
				tile.end.y = std::min(height, tile.end.y);
				mTiles.push_back(tile);
			}
		}
	}

	void View::lookFromTo(glm::vec3 from, glm::vec3 to)
	{
		mPos = from;
		float hHeight = tanf(mVFov/2.f);
		float hWidth = (float(mWidth)/float(mHeight)) * hHeight;
		glm::vec3 up{0,1,0};
		glm::vec3 w = glm::normalize(from - to);
		glm::vec3 u = glm::normalize(glm::cross(up,w));
		glm::vec3 v = glm::cross(w, u);

		mLowerLeft = glm::vec3{} - hWidth*u - hHeight*v -w;
		mHorizontal = 2.f * hWidth * u;
		mVertical = 2.f * hHeight * v;
	}

	Ray View::rayCast(float u, float v)
	{
		auto direction = mLowerLeft + (u * mHorizontal) + (v * mVertical);
		return Ray{mPos, direction};
	}

	TraceOutput View::trace(const std::function<TraceData(const Ray&, const World*, glm::tvec2<size_t>)>& task)
	{
		Image imgColor{mWidth, mHeight};
		Image imgNormal{mWidth, mHeight};
		Image imgDepth{mWidth, mHeight};

		std::mutex stackMutex;
		std::atomic<size_t> tileIndex{0};
		std::atomic_bool timeShown{false};

		auto startTime = std::chrono::high_resolution_clock::now();

		auto func = [this, &stackMutex, &tileIndex, task,
					&imgColor, &imgNormal, &imgDepth, &startTime, &timeShown](size_t tid)
		{
			const size_t sampleCount = mSampleTable.size();
			while(true)
			{
				stackMutex.lock();
				if(tileIndex.load() >= mTiles.size())
				{
					stackMutex.unlock();
					break;
				}
				auto tile = mTiles[tileIndex];

				// Progress printing
				{
					auto taken = std::chrono::high_resolution_clock::now() - startTime;

					float progress = float(tileIndex) / float(mTiles.size());
					double time = std::chrono::duration_cast<std::chrono::seconds>(taken).count();
					auto total = time/progress;
					time = total - time;
					if (taken > std::chrono::seconds(5) && (time > 5.0 || timeShown.load()))
					{
						timeShown.store(true);
						bool sign0{false};
						bool sign1{false};
						if(time < 45)
						{
							sign0 = true;
							sign1 = true;
						}
						else if(time < 75)
						{
							sign1 = true;
						}
						size_t minutes = std::ceil(time / 60.0);

						std::cout << "\r" << "Rendering " << (int)floorf(progress * 100.f) <<
								  "% | Estimated time left: " << (sign0?"<":"") << (sign1?1:minutes)
								  << " minute"<< (sign1?"":"s") <<"\t\t\t\t\t\t" << std::flush;
					}
					else
					{
						std::cout << "\r" << "Rendering "
								  << (int)floorf(progress * 100.f) << "%"
								  << std::flush;
					}
				}

				tileIndex++;
				stackMutex.unlock();

				for(size_t x = tile.begin.x; x < tile.end.x; ++x)
				{
					for(size_t y = tile.begin.y; y < tile.end.y; ++y)
					{
						glm::vec2 coord{x,y};
						glm::vec3 color{};
						glm::vec3 normal{};
						glm::vec3 depth{};
						for(size_t sample = 0; sample < sampleCount; ++sample)
						{

							float u = ((float(coord.x) + mSampleTable[sample].x) / float(mWidth));
							float v = (float(coord.y) + mSampleTable[sample].y) / float(mHeight);
							auto data = task(rayCast(u,v), mWorld, coord);
							color += data.color;
							normal = data.normal;
							depth.r += data.depth;

							//depth.b = float(tid);
							//depth.b = 1.f;
						}
						color = color / glm::vec3(sampleCount);
						depth = depth / glm::vec3(sampleCount);

						imgColor.store(coord.x, coord.y, color);
						imgNormal.store(coord.x, coord.y, normal);
						imgDepth.store(coord.x, coord.y, depth);
					}
				}
			}
		};
		if(mThreadCount > 1)
		{
			std::vector<std::thread> threads;
			for (size_t tid = 0; tid < mThreadCount; ++tid)
			{
				threads.emplace_back(func, tid);
			}
			for (auto &thread : threads)
			{
				thread.join();
			}
		}
		else
		{
			func(0);
		}
		auto taken = std::chrono::high_resolution_clock::now()-startTime;
		size_t seconds = std::ceil(std::chrono::duration_cast<std::chrono::milliseconds>(taken).count()/1000.0);
		size_t minutes = std::chrono::duration_cast<std::chrono::minutes>(taken).count();
		seconds -= minutes*60;
		std::cout << "\r" <<"Tracing completed in "<< minutes <<" minutes " << seconds <<" seconds\t\t\t" << std::endl;

		return TraceOutput{imgColor, imgNormal, imgDepth};
	}



}