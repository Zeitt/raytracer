#include <raytracer/Texture.hpp>
#include <raytracer/Image.hpp>

namespace raytracer
{
	Color::Color(glm::vec3 color):
			mData(color)
	{}
	glm::vec3 Color::value(glm::vec2, glm::vec3) const
	{
		return mData;
	}


	CheckerTexture::CheckerTexture():
			mOdd(glm::vec3{0.102f, 0.149f, 0.125f}),
			mEven(glm::vec3{0.925f, 0.945f, 0.937f})
	{}

	CheckerTexture::CheckerTexture(glm::vec3 odd, glm::vec3 even):
			mOdd(odd),
			mEven(even)
	{}

	glm::vec3 CheckerTexture::value(glm::vec2, glm::vec3 p) const
	{
		float size = 16.f;
		float sine = sinf(size*p.x)*sinf(size*p.y)*sinf(size*p.z);
		if(sine < 0.f)
		{
			return mOdd;
		}
		else
		{
			return mEven;
		}
	}


	ImageTexture::ImageTexture(Image* img):
		mImage(img)
	{}

	glm::vec3 ImageTexture::value(glm::vec2 uv, glm::vec3) const
	{
		int x = std::min(std::max((size_t)floorf(uv.x*mImage->getWidth()), size_t(0)), mImage->getWidth()-1);
		int y = std::min(std::max((size_t)floorf(uv.y*mImage->getHeight()), size_t(0)), mImage->getHeight()-1);
		return mImage->load(x,y);
	}


	NoiseTexture::NoiseTexture(float scale ):
		mScale(scale)
	{}
	glm::vec3 NoiseTexture::value(glm::vec2, glm::vec3 p) const
	{
		return glm::vec3{1.f} * mRand.perlinNoise(p*mScale);
	}


	MarbleTexture::MarbleTexture(float scale ):
		mScale(scale)
	{}
	glm::vec3 MarbleTexture::value(glm::vec2, glm::vec3 p) const
	{
		return glm::vec3{1.f} * 0.5f * (1.f + sinf(mScale*p.z+10.f*mRand.perlinTurb(p*mScale)));
	}
}