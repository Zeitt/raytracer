#include <raytracer/Sphere.hpp>

namespace raytracer
{
	Sphere::Sphere(glm::vec3 position, float radius, Material* material):
			Object(material),
			mPos(position),
			mRadius(radius)
	{}

	bool Sphere::hitTest(const Ray& r, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		auto oc = r.getOrigin() - mPos;
		float a = glm::dot(r.getDirection(), r.getDirection());
		float b = glm::dot(oc, r.getDirection());
		float c = glm::dot(oc,oc) - (mRadius*mRadius);
		float discriminant = b*b - a*c;
		if(discriminant > 0.f)
		{
			float temp = (-b - sqrtf(b*b-a*c))/a;
			if(temp < maxDistance && temp > minDistance)
			{
				recordOut.distance = temp;
				recordOut.point = r.pointAt(temp);
				recordOut.uv = getUV(recordOut.point);
				recordOut.normal = ((recordOut.point - mPos) / mRadius);
				recordOut.material = mMaterial;
				return true;
			}
			temp = (-b + sqrtf(b*b-a*c))/a;
			if(temp < maxDistance && temp > minDistance)
			{
				recordOut.distance = temp;
				recordOut.point = r.pointAt(temp);
				recordOut.uv = getUV(recordOut.point);
				recordOut.normal = ((recordOut.point - mPos) / mRadius);
				recordOut.material = mMaterial;
				return true;
			}
		}

		return false;
	}

	AABB Sphere::getAABB() const
	{
		glm::vec3 min = mPos - glm::vec3{fabsf(mRadius)};
		glm::vec3 max = mPos + glm::vec3{fabsf(mRadius)};
		return AABB{min,max};
	}

	/// Private

	glm::vec2 Sphere::getUV(glm::vec3 p) const
	{
		p = (p-mPos)/mRadius;
		float phi = atan2f(p.z, p.x);
		float theta = asinf(p.y);
		glm::vec2 uv{
			1.f - (phi + M_PI) / (2.f * M_PI),
			(theta + M_PI / 2) / M_PI
		};
		return uv;
	}
}