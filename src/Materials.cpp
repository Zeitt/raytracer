#include <raytracer/Materials.hpp>
#include <raytracer/Sphere.hpp>

namespace raytracer
{
	/// Lambertian material
	Lambertian::Lambertian(Texture* diffuse):
		mDiffuse(diffuse)
	{ }

	bool Lambertian::scatter(const Ray& ray, const HitRecord& record, glm::vec3& attenuation, Ray& scattered) const
	{
		glm::vec3 target = record.point + record.normal + mRandomizer.randomInUnitSphere();
		scattered = Ray{record.point, target-record.point};
		attenuation = mDiffuse->value(record.uv, record.point);
		return true;
	}

	/// Metallic material
	Metal::Metal(Texture* diffuse, float roughness):
		mAlbedo(diffuse),
		mRoughness(roughness)
	{ }
	bool Metal::scatter(const Ray& ray, const HitRecord& record, glm::vec3& attenuation, Ray& scattered) const
	{
		glm::vec3 reflected = glm::reflect(glm::normalize(ray.getDirection()), record.normal);
		scattered = Ray{record.point, reflected + (mRoughness * mRandomizer.randomInUnitSphere())};
		attenuation = mAlbedo->value( record.uv , record.point);
		return (glm::dot(scattered.getDirection(), record.normal) > 0.f);
	}

	/// Dielectric
	Dielectric::Dielectric(float reflection):
			mReflection(reflection)
	{ }

	bool Dielectric::scatter(const Ray& ray, const HitRecord& record, glm::vec3& attenuation, Ray& scattered) const
	{
		glm::vec3 reflected = glm::reflect(ray.getDirection(), record.normal);
		attenuation = mColor;

		glm::vec3 outwardNormal;
		float niOverNt{};
		float cosine{};
		if(glm::dot(ray.getDirection(), record.normal) > 0)
		{
			outwardNormal = -record.normal;
			niOverNt = mReflection;
			cosine = mReflection * glm::dot(ray.getDirection(), record.normal) / glm::length(ray.getDirection());
		}
		else
		{
			outwardNormal = record.normal;
			niOverNt = 1.f/fmaxf(mReflection, 0.001f);
			cosine = -glm::dot(ray.getDirection(), record.normal) / glm::length(ray.getDirection());
		}

		float reflectProp;
		glm::vec3 refracted;
		if(refract(ray.getDirection(), outwardNormal, niOverNt,refracted))
		{
			reflectProp = schlick(cosine, mReflection);
		}
		else
		{
			reflectProp = 1.f;
		}
		glm::vec3 direction{};
		if(mRandomizer.randomFloat() < reflectProp)
		{
			direction = reflected;
		}
		else
		{
			direction = refracted;
		}
		scattered = Ray{record.point, direction};
		return true;
	}

	//private
	bool Dielectric::refract(glm::vec3 v, glm::vec3 normal, float ni, glm::vec3& refracted) const
	{
		auto uv = glm::normalize(v);
		float dt = glm::dot(uv, normal);
		float discriminant = 1.f - ni*ni*(1-dt*dt);
		if(discriminant > 0.f)
		{
			refracted = ni * (uv-normal*dt) - normal * sqrtf(discriminant);
			return true;
		}
		return false;
	}
	float Dielectric::schlick(float cosine, float refl) const
	{
		float r0 = (1.f-refl) / (1.f+refl);
		r0 *= r0;
		return r0 + (1-r0)*powf((1.f-cosine),5.f);
	}
}