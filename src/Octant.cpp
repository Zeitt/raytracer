#include <raytracer/Octant.hpp>

namespace raytracer
{
	void Octant::build(AABB aabb, std::vector<Object*>& objs, size_t maxDepth)
	{
		const bool hasChildren = maxDepth > 0 && objs.size() > 8;// also check how many objects are left
		mAABB = aabb;

		if(hasChildren)
		{
			mChildren.resize(8);

			glm::vec3 wDim = (mAABB.max - mAABB.min) / glm::vec3{2};
			/* Slower than going trough loop only once but more readable and this is only done once, so overhead is not too large*/
			unsigned childIndex = 0;
			for (unsigned z = 0; z < 2; ++z)
			{
				for (unsigned y = 0; y < 2; ++y)
				{
					for (unsigned x = 0; x < 2; ++x)
					{
						AABB newAABB{};
						newAABB.min =
						{
							mAABB.min.x + (wDim.x * float(x)),
							mAABB.min.y + (wDim.y * float(y)),
							mAABB.min.z + (wDim.z * float(z)),
						};
						newAABB.max =
						{
							mAABB.min.x + (wDim.x * (x+1.f)),
							mAABB.min.y + (wDim.y * (y+1.f)),
							mAABB.min.z + (wDim.z * (z+1.f)),
						};
						std::vector<Object*> subObjects;
						subObjects.reserve(objs.size());
						for (auto & obj : objs)
						{
							if(AABBsOverlap(newAABB, obj->getAABB()))
							{
								subObjects.push_back(obj);
							}
						}
						mChildren[childIndex++].build(newAABB, subObjects, maxDepth - 1);
					}
				}
			}
		}
		else
		{
			mObjects = objs;
		}
	}

	namespace
	{
		bool aabbHitTest(AABB aabb, const Ray& ray, float minDistance, float maxDistance)
		{
			for(size_t st = 0; st < 3; ++st)
			{
				float t0 = std::min((aabb.min[st] - ray.getOrigin()[st]) / (ray.getDirection()[st]),
				                    (aabb.max[st] - ray.getOrigin()[st])/ (ray.getDirection()[st]));
				float t1 = std::max((aabb.min[st] - ray.getOrigin()[st]) / (ray.getDirection()[st]),
				                    (aabb.max[st] - ray.getOrigin()[st])/ (ray.getDirection()[st]));
				minDistance = std::max(t0, minDistance);
				maxDistance = std::min(t1, maxDistance);
				if(maxDistance <= minDistance)
				{
					return false;
				}
			}
			return true;
		}
	}

	bool Octant::hitTest(const Ray& ray, float minDistanceTest, float maxDistanceTest, HitRecord &recordOut) const
	{
		bool hitAnything{false};
		HitRecord record;
		bool hitThis = aabbHitTest(mAABB, ray, minDistanceTest, maxDistanceTest);
		if(hitThis)
		{
			for(auto& bvh : mChildren)
			{
				if(bvh.hitTest(ray, minDistanceTest, maxDistanceTest, record))
				{
					hitAnything = true;
					recordOut = record;
				}
			}
			float closest = maxDistanceTest;
			for (Object *obj : mObjects)
			{
				if (obj->hitTest(ray, minDistanceTest, closest, record))
				{
					hitAnything = true;
					recordOut = record;
					closest = record.distance;
				}
			}
		}
		return hitAnything;
	}
}