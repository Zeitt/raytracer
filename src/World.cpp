#include <raytracer/World.hpp>
#include <iostream>

namespace raytracer
{
	/// PUBLIC

	World::World(float minTestDepth, float maxTestDepth):
				mMinTest(minTestDepth),
				mMaxTest(maxTestDepth)
	{}

	View World::createView(float fov, size_t renderWidth, size_t renderHeight, size_t sampleCount, bool useThreading)
	{
		return View(this, fov, renderWidth, renderHeight, sampleCount, useThreading);
	}

	void World::buildOctree(size_t maxDepth)
	{
		if(!mOctreeBuild)
		{
			std::cout << "Building Octree" << std::flush;
			std::vector<Object*> objects;
			AABB octreeAABB;
			octreeAABB.min = glm::vec3{MAXFLOAT,MAXFLOAT,MAXFLOAT};
			octreeAABB.max = glm::vec3{-MAXFLOAT,-MAXFLOAT,-MAXFLOAT};
			for(auto& obj : mObjects)
			{
				auto aabb = obj->getAABB();
				octreeAABB.min = glm::min(aabb.min, octreeAABB.min);
				octreeAABB.max = glm::max(aabb.max, octreeAABB.max);

				objects.emplace_back(obj.get());
			}
			mOctree.build(octreeAABB, objects, maxDepth);
			mOctreeBuild = true;
			std::cout << "\rOctree build!\t" << std::endl;
		}
		else
		{
			std::cout << "Octree already build!\n";
		}
	}

	bool World::hitTest(const Ray ray, HitRecord &recordOut) const
	{
		bool hitAnything{false};
		float closest = mMaxTest;
		HitRecord record;
		if(mOctreeBuild)
		{
			hitAnything = mOctree.hitTest(ray, mMinTest, closest, record);
			recordOut = record;
		}
		else
		{
			for (auto& obj : mObjects)
			{
				if (obj->hitTest(ray, mMinTest, closest, record))
				{
					hitAnything = true;
					closest = record.distance;
					recordOut = record;
				}
			}
		}
		return hitAnything;
	}

	glm::vec3 World::getSkyColor(const Ray& ray) const
	{
		glm::vec3 normDir = glm::normalize(ray.getDirection());
		float distance = 0.5f * (normDir.y + 1.f);
		glm::vec3 colorBegin = colors::gSkyBlue();
		glm::vec3 colorEnd = colors::gAzureWhite();
		return glm::lerp(colorEnd, colorBegin, distance);
	}
}