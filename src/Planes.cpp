#include <raytracer/Planes.hpp>

namespace raytracer
{
	PlaneXY::PlaneXY(glm::vec2 begin, glm::vec2 end, float k, Material* mat):
		Object(mat),
		mOrigin(begin),mEnd(end), mK(k)
	{}
	bool PlaneXY::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		float distance= (mK - ray.getOrigin().z) / ray.getDirection().z;
		if(distance < minDistance || distance > maxDistance)
			return false;
		float x = ray.getOrigin().x + distance* ray.getDirection().x;
		float y = ray.getOrigin().y + distance* ray.getDirection().y;
		if((x < mOrigin.x || x > mEnd.x) ||
		   (y < mOrigin.y || y > mEnd.y))
		{
			return false;
		}
		auto dim = mEnd-mOrigin;
		recordOut.uv = glm::vec2{(x-mOrigin.x)/dim.x, (y-mOrigin.y)/dim.y};
		recordOut.distance = distance;
		recordOut.material = mMaterial;
		recordOut.point = ray.pointAt(distance);
		recordOut.normal = glm::vec3(0,0,1);
		return true;
	}

	PlaneXZ::PlaneXZ(glm::vec2 begin, glm::vec2 end, float k, Material* mat):
		Object(mat),
		mOrigin(begin),mEnd(end), mK(k)
	{}
	bool PlaneXZ::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		float distance= (mK - ray.getOrigin().y) / ray.getDirection().y;
		if(distance < minDistance || distance> maxDistance)
			return false;
		float x = ray.getOrigin().x + distance* ray.getDirection().x;
		float z = ray.getOrigin().z + distance* ray.getDirection().z;
		if((x < mOrigin.x || x > mEnd.x) ||
		   (z < mOrigin.y || z > mEnd.y))
		{
			return false;
		}
		auto dim = mEnd-mOrigin;
		recordOut.uv = glm::vec2{(x-mOrigin.x)/dim.x, (z-mOrigin.y)/dim.y};
		recordOut.distance = distance;
		recordOut.material = mMaterial;
		recordOut.point = ray.pointAt(distance);
		recordOut.normal = glm::vec3(0,1, 0);
		return true;
	}

	PlaneYZ::PlaneYZ(glm::vec2 begin, glm::vec2 end, float k, Material* mat):
		Object(mat),
		mOrigin(begin),mEnd(end), mK(k)
	{}
	bool PlaneYZ::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		float distance= (mK - ray.getOrigin().x) / ray.getDirection().x;
		if(distance < minDistance || distance> maxDistance)
			return false;
		float y = ray.getOrigin().y + distance* ray.getDirection().y;
		float z = ray.getOrigin().z + distance* ray.getDirection().z;
		if((y < mOrigin.x || y > mEnd.x) ||
		   (z < mOrigin.y || z > mEnd.y))
		{
			return false;
		}
		auto dim = mEnd-mOrigin;
		recordOut.uv = glm::vec2{(y-mOrigin.x)/dim.x, (z-mOrigin.y)/dim.y};
		recordOut.distance = distance;
		recordOut.material = mMaterial;
		recordOut.point = ray.pointAt(distance);
		recordOut.normal = glm::vec3(1, 0, 0);
		return true;
	}
}