#include <raytracer/Utils.hpp>

namespace raytracer
{
	Randomizer::Randomizer() :
		mDistribution(0.f, 1.f)
	{
		std::random_device rand;
		mGenerator = std::mt19937(rand());

		mPerlin.init(this);
	}

	float Randomizer::randomFloat()
	{
		return mDistribution(mGenerator);
	}

	glm::vec2 Randomizer::randomInPixel()
	{
		return glm::vec2{mDistribution(mGenerator), mDistribution(mGenerator)};
	}

	glm::vec3 Randomizer::randomInUnitSphere()
	{
		glm::vec3 point{};
		do
		{
			point = glm::vec3{mDistribution(mGenerator), mDistribution(mGenerator), mDistribution(mGenerator)};
			point = (2.f * point) - glm::vec3{1.f};
		} while (glm::dot(point,point) >= 1.f);

		return point;
	}

	/// Perlin noise related things

	void Randomizer::Perlin::init(Randomizer* randomizer)
	{
		mFloats.resize(256);
		mPerm.resize(256);
		for(int64_t st = 0; st < 256; ++st)
		{
			mFloats[st] = glm::vec3{-1 + 2*randomizer->randomFloat(),
									-1 + 2*randomizer->randomFloat(),
									-1 + 2*randomizer->randomFloat()};
			mFloats[st] = glm::normalize(mFloats[st]);
			mPerm[st] = glm::tvec3<int64_t>{st};
		}
		for(size_t i = 255; i >0; --i)
		{
			size_t target = floorf(randomizer->randomFloat()*(i+1.f));
			target = std::min(target, size_t(255));
			size_t temp = mPerm[i].x;
			mPerm[i].x = mPerm[target].x;
			mPerm[target].x = temp;

			target = floorf(randomizer->randomFloat()*(i+1.f));
			temp = mPerm[i].y;
			mPerm[i].y = mPerm[target].y;
			mPerm[target].y = temp;

			target = floorf(randomizer->randomFloat()*(i+1.f));
			temp = mPerm[i].z;
			mPerm[i].z = mPerm[target].z;
			mPerm[target].z = temp;
		}
	}

	float perlinInterp(glm::vec3 c[2][2][2], glm::vec3 uvw)
	{
		float uu = uvw.x*uvw.x*(3-2*uvw.x);
		float vv = uvw.y*uvw.y*(3-2*uvw.y);
		float ww = uvw.z*uvw.z*(3-2*uvw.z);
		float accum{};
		for (size_t i=0; i < 2; i++)
		{
			for (size_t j = 0; j < 2; j++)
			{
				for (size_t k = 0; k < 2; k++)
				{
					glm::vec3 weight_v(uvw.x - i, uvw.y - j, uvw.z - k);
					accum += (i * uu + (1.f - i) * (1 - uu)) *
							 (j * vv + (1.f - j) * (1 - vv)) *
							 (k * ww + (1.f - k) * (1 - ww)) *
							 dot(c[i][j][k], weight_v);
				}
			}
		}
		return accum;
	}
	float Randomizer::Perlin::noise(glm::vec3 p) const
	{
		glm::vec3 uvw{
			p.x - floorf(p.x),
			p.y - floorf(p.y),
			p.z - floorf(p.z)
		};
		int i = int(floorf(p.x));
		int j = int(floorf(p.y));
		int k = int(floorf(p.z));
		glm::vec3 c[2][2][2];
		for(size_t di=0; di < 2; di++)
		{
			for(size_t dj=0; dj < 2; dj++)
			{
				for(size_t dk=0; dk < 2; dk++)
				{
					c[di][dj][dk] = mFloats[
							mPerm[(i+di) & 255U].x ^
							mPerm[(j+dj) & 255U].y ^
							mPerm[(k+dk) & 255U].z
						];
				}
			}
		}

		return perlinInterp(c, uvw);
	}
	float Randomizer::Perlin::turb(glm::vec3 p, size_t depth) const
	{
		float accum{};
		glm::vec3 temp{p};
		float weight = 1.0;
		for (size_t i = 0; i < depth; i++)
		{
			accum += weight*noise(temp);
			weight *= 0.5;
			temp *= 2;
		}
		return fabsf(accum);
	}
}