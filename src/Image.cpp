#include <raytracer/Image.hpp>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace raytracer
{
	/// Public

	Image::Image(size_t width, size_t height):
		mWidth(width),
		mHeight(height),
		mData(width*height)
	{}


	Image::Image(const char* path)
	{
		int w{},h{},c{};
		auto data = stbi_load(path,&w,&h,&c,3);
		assert(data != nullptr);

		mWidth = w;
		mHeight = h;
		mData.resize(w*h);
		for(size_t y = 0; y < mHeight; ++y)
		{
			for (size_t x = 0; x < mWidth; ++x)
			{
				size_t posOut = x + (y*h);
				size_t posIn = (x + (y*h))*3;
				mData[posOut].r = float(data[posIn+0])/255.f;
				mData[posOut].g = float(data[posIn+1])/255.f;
				mData[posOut].b = float(data[posIn+2])/255.f;
			}
		}
		stbi_image_free(data);
	}

	void Image::store(size_t x, size_t y, glm::vec3 normalizedValue)
	{
		assert(x < mWidth && y < mHeight);
		const size_t pos = x + (y*mWidth);
		mData[pos] = normalizedValue;
	}

	glm::vec3 Image::load(size_t x, size_t y) const
	{
		assert(x < mWidth && y < mHeight);
		return mData[x + (y*mWidth)];
	}

	bool Image::saveHDR(const std::string& path) const
	{
		return stbi_write_hdr(path.c_str(), mWidth, mHeight, 3, (float*)mData.data()) == 1;
	}
	bool Image::savePNG(const std::string& path) const
	{
		std::vector<uint8_t> data;
		data.resize(mWidth*mHeight*3);
		for(size_t y = 0; y < mHeight; ++y)
		{
			for(size_t x = 0; x < mWidth; ++x)
			{
				auto val = clamp(load(x,y));
				size_t pos = (x + ((mHeight-y-1) * mWidth))*3;
				data[pos+0] = val.x;
				data[pos+1] = val.y;
				data[pos+2] = val.z;
			}
		}
		return stbi_write_png(path.c_str(), mWidth, mHeight, 3, data.data(), 0) == 1;
	}

	/// PRIVATE

	glm::tvec3<uint8_t> Image::clamp(glm::vec3 col) const
	{
		return {
			lround(std::min(255.f , std::max(0.f, col.r*255.f))),
			lround(std::min(255.f , std::max(0.f, col.g*255.f))),
			lround(std::min(255.f , std::max(0.f, col.b*255.f)))
		};
	}
}