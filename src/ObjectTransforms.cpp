#include <raytracer/ObjectTransforms.hpp>

namespace raytracer
{
	/// Flipped Object /////////////////////////////////////////////////////////////////////////////////////////////////

	Flipped::Flipped(const Object& obj):
		Object(nullptr),
		mObj(obj)
	{}

	Flipped::~Flipped() = default;

	bool Flipped::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		if(mObj.hitTest(ray,minDistance,maxDistance,recordOut))
		{
			recordOut.normal = -recordOut.normal;
			return true;
		}
		return false;
	}

	inline AABB Flipped::getAABB() const
	{
		return mObj.getAABB();
	}

	/// Y-Rotated Object ///////////////////////////////////////////////////////////////////////////////////////////////
	YRotated::YRotated(const Object& obj, float degreeAngle):
		Object(nullptr),
		mObj(obj),
		mAngle(glm::radians(degreeAngle))
	{
		mAABB = obj.getAABB();
		mThetaSin = sinf(mAngle);
		mThetaCos = cosf(mAngle);
		glm::vec3 min{MAXFLOAT};
		glm::vec3 max{-MAXFLOAT};
		for(size_t i = 0; i < 2; ++i)
		{
			for(size_t j = 0; j < 2; ++j)
			{
				for(size_t k = 0; k < 2; ++k)
				{
					float x = i*mAABB.max.x + (1.f-i)*mAABB.min.x;
					float y = j*mAABB.max.y + (1.f-j)*mAABB.min.y;
					float z = k*mAABB.max.z + (1.f-k)*mAABB.min.z;
					glm::vec3 tester{
						mThetaCos*x + mThetaSin*z,
						y,
						-mThetaSin*x + mThetaCos*z
					};
					for(size_t st = 0; st < 3; ++st)
					{
						if(tester[st] > max[st])
							max[st] = tester[st];
						if(tester[st] < min[st])
							min[st] = tester[st];
					}
				}
			}
		}
		mAABB.max = max;
		mAABB.min = min;
	}

	YRotated::~YRotated() = default;

	inline bool YRotated::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		glm::vec3 origin{
			mThetaCos * ray.getOrigin().x - mThetaSin * ray.getOrigin().z,
			ray.getOrigin().y,
			mThetaSin * ray.getOrigin().x + mThetaCos * ray.getOrigin().z
		};
		glm::vec3 direction{
			mThetaCos * ray.getDirection().x - mThetaSin * ray.getDirection().z,
			ray.getDirection().y,
			mThetaSin * ray.getDirection().x + mThetaCos * ray.getDirection().z
		};
		Ray rotated{origin,direction};
		if(mObj.hitTest(rotated,minDistance,maxDistance,recordOut))
		{
			glm::vec3 p{
				mThetaCos*recordOut.point.x + mThetaSin*recordOut.point.z,
				recordOut.point.y,
				-mThetaSin*recordOut.point.x + mThetaCos*recordOut.point.z
			};
			glm::vec3 n{
				mThetaCos*recordOut.normal.x + mThetaSin*recordOut.normal.z,
				recordOut.normal.y,
				-mThetaSin*recordOut.normal.x + mThetaCos*recordOut.normal.z
			};
			recordOut.point = p;
			recordOut.normal = n;
			return true;
		}
		return false;
	}

	inline AABB YRotated::getAABB() const
	{
		return mAABB;
	}

	/// Translated Object //////////////////////////////////////////////////////////////////////////////////////////////
	Translated::Translated(const Object& obj, glm::vec3 offset):
		Object(nullptr),
		mOffset(offset),
		mObj(obj)
	{ }

	bool Translated::hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const
	{
		Ray moved{ray.getOrigin() - mOffset, ray.getDirection()};
		if(mObj.hitTest(moved, minDistance, maxDistance, recordOut))
		{
			recordOut.point += mOffset;
			return true;
		}
		return false;
	}

	inline AABB Translated::getAABB() const
	{
		return AABB{mObj.getAABB().min + mOffset, mObj.getAABB().max + mOffset};
	}
}