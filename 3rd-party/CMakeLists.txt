cmake_minimum_required(VERSION 3.14)

project(glm)
set(CMAKE_CXX_STANDARD 14)
add_library(glm INTERFACE)
target_include_directories(glm INTERFACE glm)


project(stb)
set(CMAKE_CXX_STANDARD 14)
add_library(stb INTERFACE)
target_include_directories(stb INTERFACE stb)