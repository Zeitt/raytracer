#include "examples/CornellsBox.h"
#include "examples/BallWorld.h"

#include <raytracer/Ray.hpp>
#include <raytracer/Sphere.hpp>
#include <raytracer/World.hpp>
#include <raytracer/View.hpp>

#include <iostream>

/**
 * @brief Recursive shading function.
 * @param ray Ray either from camera or given as argument.
 * @param world World containing objects
 * @param coord Screen-space camera coordinate.
 * @param bounces How many bounces are still left.
 * @return Output of hit
 */
raytracer::TraceData Shade(const raytracer::Ray& ray, const raytracer::World* world, glm::tvec2<size_t> coord, int64_t bounces)
{
	raytracer::TraceData out{};
	raytracer::HitRecord record{};
	if (world->hitTest(ray, record))
	{
		out.normal = record.normal;
		out.depth = record.distance / world->getMaxDepth();

		raytracer::Ray scattered{{},{}};
		glm::vec3 attenuation{};
		out.color = record.material->emitted(record.uv, record.point);
		if(bounces > 0 && record.material->scatter(ray, record,attenuation,scattered))
		{
			auto res = Shade(scattered, world, coord, bounces - 1);
			out.color +=  attenuation * res.color;
		}
	}
	else
	{
		out.normal = glm::vec3{1.f};
		out.color = world->getSkyColor(ray)/3.f;
		out.depth = 1.f;
	}
	return out;
}

/**
 * @brief Actual starting point for raytracing. Given in argument to View::trace()
 * @param ray Ray from camera.
 * @param world World containing objects.
 * @param coord Screen space coordinate.
 * @return Result of raytracing.
 */
raytracer::TraceData TraceFunc(const raytracer::Ray& ray, const raytracer::World* world, glm::tvec2<size_t> coord)
{
	int64_t bounces = 5;
	return Shade(ray, world, coord, bounces);
}

int main(int argumentCount, char* arguments[])
{
	std::cout << "Loading scene\n";
	float fov = 40.f;

	std::string runningExample = "CornellsBox";
//#define DEFAULT_SCENE_BALLS
#ifdef DEFAULT_SCENE_BALLS
	runningExample = "BallWorld";
#endif //DEFAULT_SCENE_BALLS

	for(int argumentIndex = 0; argumentIndex < argumentCount; ++argumentIndex)
	{
		if(std::string(arguments[argumentIndex]) == "BallWorld")
		{
			runningExample = "BallWorld";
		}
		else if(std::string(arguments[argumentIndex]) == "CornellsBox")
		{
			runningExample = "CornellsBox";
		}
	}

	std::cout << "Creating '" + runningExample + "'\n";

	raytracer::TraceOutput output;
	if(runningExample == "CornellsBox")
	{
		CornellsBox box{};
		auto& world = box.world;
		raytracer::View view = world.createView(fov, 512, 512, 512);
		view.lookFromTo({278,278,-780}, {278,278,0});
		output = view.trace(TraceFunc);
	}
	else
	{
		BallWorld balls{1000,25};
		auto& world = balls.world;
		raytracer::View view = world.createView(fov, 1280, 720, 512);
		std::cout << "Building Octree.\n";
		balls.world.buildOctree();
		view.lookFromTo({5,5,5}, {0,0,0});
		std::cout << "Starting raytracing.\n";
		output = view.trace(TraceFunc);
	}

	std::cout << "Saving results\n";

	// Using PNG as it can be previewed in CLion automatically
	output.color.savePNG("preview.png");
	output.color.saveHDR("colors.hdr");
	output.normal.savePNG("normals.png");
	output.depth.saveHDR("depth.hdr");

	return 0;
}
