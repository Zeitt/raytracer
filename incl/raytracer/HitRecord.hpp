#ifndef RAYTRACER_HITRECORD_HPP
#define RAYTRACER_HITRECORD_HPP

#include <glm.hpp>

namespace raytracer
{
// Predeclared to use within Object-class. See Material.hpp for details.
class Material;

/**
 * @brief Structure for passing data between ray hits.
 */
struct HitRecord final
{
	float distance{};

	glm::vec2 uv{};
	glm::vec3 point{};
	glm::vec3 normal{};

	Material const* material{};
};
}

#endif //RAYTRACER_HITRECORD_HPP