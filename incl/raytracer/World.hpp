#ifndef RAYTRACER_WORLD_HPP
#define RAYTRACER_WORLD_HPP

#include <raytracer/Object.hpp>
#include <raytracer/Sphere.hpp>
#include <raytracer/View.hpp>
#include <raytracer/Box.hpp>
#include <raytracer/Octant.hpp>

#include <glm.hpp>

#include <vector>

namespace raytracer
{
	/**
	 * @brief World. All objects are placed here, as well as views are taken from this.
	 */
	class World final
	{
	public:
		/**
		 * @brief Creates new World.
		 * @param minTestDepth Minimum distance from previous hit to test.
		 * @param maxTestDepth Maximum distance from previous hit to test.
		 */
		explicit World(float minTestDepth, float maxTestDepth);

		/**
		 * @brief Creates View to world. Used to trace result.
		 * @param fov Vertical Field of view in radians.
		 * @param renderWidth How many pixels in x-axis.
		 * @param renderHeight How many pixels in y-axis.
		 * @param sampleCount Optional, default = 4. How many rays/samples will be cast per pixel.
		 * @param useThreading Optional, default = true. Should tracing be multi-threaded.
		 * @return View to World.
		 */
		View createView(float fov, size_t renderWidth, size_t renderHeight, size_t sampleCount = 4, bool useThreading = true);

		/**
		 * @brief Tests will ray hit anything inside world.
		 * @param ray Input ray.
		 * @param recordOut Output result.
		 * @return true if hit, false otherwise.
		 */
		bool hitTest(const Ray ray, HitRecord &recordOut) const;

		/**
		 * @brief Adds Sphere -object to scene. See Sphere::Sphere() for details.
		 */
		template<typename ...Args>
		bool addSphere(Args... args)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new Sphere(args...));
			}
			return !mOctreeBuild;
		}
		/**
		 * @brief Adds Box -object to scene. See Box::Box() for details.
		 */
		template<typename ...Args>
		bool addBox(Args... args)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new Box(args...));
			}
			return !mOctreeBuild;
		}

		/**
		 * @brief Adds Plane -object to scene. See PlaneXY::PlaneXY() for details.
		 */
		template<typename ...Args>
		bool addPlaneXY(Args... args)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new PlaneXY(args...));
			}
			return !mOctreeBuild;
		}
		template<typename ...Args>
		/**
		 * @brief Adds Plane -object to scene. See PlaneXZ::PlaneXZ() for details.
		 */
		bool addPlaneXZ(Args... args)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new PlaneXZ(args...));
			}
			return !mOctreeBuild;
		}
		template<typename ...Args>
		/**
		 * @brief Adds Plane -object to scene. See PlaneYZ::PlaneYZ() for details.
		 */
		bool addPlaneYZ(Args... args)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new PlaneYZ(args...));
			}
			return !mOctreeBuild;
		}

		/**
		 * @copybrief Flipped::Flipped()
		 */
		bool addFlipped(const Object& obj)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new Flipped(obj));
			}
			return !mOctreeBuild;
		}
		/**
		 * @copybrief YRotated::YRotated()
		 */
		bool addYRotated(const Object& obj, float angleInDegrees)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new YRotated(obj, angleInDegrees));
			}
			return !mOctreeBuild;
		}
		/**
		 * @copybrief Translated::Translated()
		 */
		bool addTranslated(const Object& obj, glm::vec3 offset)
		{
			assert(!mOctreeBuild);
			if(!mOctreeBuild)
			{
				mObjects.emplace_back(new Translated(obj, offset));
			}
			return !mOctreeBuild;
		}

		/**
		 * @brief Getter for maximum depth of scene given during construction.
		 * @return Maximum depth.
		 */
		inline float getMaxDepth() const
		{
			return mMaxTest;
		}

		/**
		 * @brief Calculates color of sky at point.
		 * @param ray Ray to test for color.
		 * @return Color of sky at certain point.
		 */
		glm::vec3 getSkyColor(const Ray& ray) const;

		/**
		 * @brief builds Octree to accelerate hit testing.
		 * @param maxDepth Optional, default 8. Generated depth of sub-splits.
		 */
		void buildOctree(size_t maxDepth = 8);
	private:
		float mMinTest{0.01f};
		float mMaxTest{100.f};

		std::vector<Sphere> mSpheres;
		std::vector<std::unique_ptr<Object>> mObjects;

		bool mOctreeBuild{false};
		Octant mOctree{}; //Bastardized k-d tree / octree
	};
}

#endif //RAYTRACER_WORLD_HPP
