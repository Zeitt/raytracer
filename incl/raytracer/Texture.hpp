#ifndef RAYTRACER_TEXTURE_HPP
#define RAYTRACER_TEXTURE_HPP

#include <raytracer/Utils.hpp>

#include <glm.hpp>

namespace raytracer
{
/**
 * @brief Base class for all textures. Can be used on objects via Material
 */
class Texture
{
public:
	/**
	 * @brief Loads value from pixel or point.
	 * @param uv 2D Coordinate where to load.
	 * @param point 3D coordinate where to load.
	 * @return Loaded value.
	 */
	virtual glm::vec3 value(glm::vec2 uv, glm::vec3 point) const = 0;
	/**
	 * @brief Destructor.
	 */
	virtual ~Texture() = default;
};

/**
 * @brief Texture containing single color.
 */
class Color final : public Texture
{
public:
	/**
	 * @brief Default constructor. Creates Black texture.
	 */
	Color() = default;
	/**
	 * @brief Creates single colored texture.
	 * @param color Single color filling texture.
	 */
	explicit Color(glm::vec3 color);

	/**
	 * @copydoc Texture::value()
	 */
	glm::vec3 value(glm::vec2 uv, glm::vec3) const final;
private:
	glm::vec3 mData{};
};

/**
 * @brief Texture with repeating checkerboard pattern.
 */
class CheckerTexture final : public Texture
{
public:
	/**
	 * @brief Default constructor. Creates checkerboard pattern with almost white and almost black colors.
	 */
	CheckerTexture();
	/**
	 * @brief Constructor for creating checkerboard pattern with custom colors.
	 * @param odd First color.
	 * @param even Second color.
	 */
	CheckerTexture(glm::vec3 odd, glm::vec3 even);
	/**
	 * @copydoc Texture::value()
	 */
	glm::vec3 value(glm::vec2, glm::vec3 p) const final;
private:
	glm::vec3 mOdd{};
	glm::vec3 mEven{};
};

// Image Predeclared to avoid conflicts
class Image;
/**
 * @brief Texture with image.
 */
class ImageTexture final : public Texture
{
public:
	/**
	 * Default constructor. Shouldnt be used.
	 */
	ImageTexture() = default;
	/**
	 * Creates Texture from Image.
	 * @param img Image used for creation.
	 */
	explicit ImageTexture(Image* img);
	/**
	 * @copydoc Texture::value()
	 */
	glm::vec3 value(glm::vec2, glm::vec3 p) const final;
private:
	Image* mImage{};
};

/**
 * @brief Greyscale Noise texture.
 */
class NoiseTexture final : public Texture
{
public:
	/**
	 * @brief Creates greyscale texture with randomizer.
	 * @param scale Optional, defaults to 1.0f. Scale for noise.
	 */
	explicit NoiseTexture(float scale = 1.f);
	/**
	 * @copydoc Texture::value()
	 */
	glm::vec3 value(glm::vec2, glm::vec3 p) const final;
private:
	float mScale{1.f};
	mutable Randomizer mRand;
};

/**
 * @brief "Marble" like texture.
 */
class MarbleTexture final : public Texture
{
public:
	/**
	 * @brief Creates "Marble"-like texture using randomizer.
	 * @param scale Optional, defaults to 1.0f. Scale used for marble.
	 */
	explicit MarbleTexture(float scale = 1.f);
	/**
	 * @copydoc Texture::value()
	 */
	glm::vec3 value(glm::vec2, glm::vec3 p) const final;
private:
	float mScale{1.f};
	mutable Randomizer mRand;
};
}

#endif //RAYTRACER_TEXTURE_HPP
