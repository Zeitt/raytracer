#ifndef RAYTRACER_OCTANT_HPP
#define RAYTRACER_OCTANT_HPP

#include <raytracer/Object.hpp>

#include <glm.hpp>
#include <vector>

namespace raytracer
{
	/**
	 * @brief Structure used to accelerate hit testing by avoiding unneeded object hits.
	 */
	class Octant final
	{
	public:
		/**
		 * Default constructor.
		 */
		explicit Octant() = default;

		/**
		 * @brief Tests will ray hit any object within this or child octants.
		 * @param minDistance Closest distance to test.
		 * @param maxDistance Furthest distance to test.
		 * @param recordOut HitRecord of results
		 * @return True if hit occurred and recordOut was modified. Otherwise false.
		 */
		bool hitTest(const Ray&, float minDistance, float maxDistance, HitRecord &recordOut) const;
		/**
		 * @brief Axis Aligned Bounding Box of Octant.
		 * @return Axis Aligned Bounding Box. Only valid after Octant::build() has been called.
		 */
		inline AABB getAABB() const { return mAABB; }
		/**
		 * @brief Used to build recursive structure from input objects.
		 * @param objs Objects which will be used to build structure.
		 * @param maxDepth Optional, defaults to 8. Maximum depth of child Octants to be generated.
		 */
		void build(AABB aabb, std::vector<Object*>& objs, size_t maxDepth = 8);
	private:
		AABB mAABB{};
		std::vector<Object*> mObjects;
		// Vector instead of array, so Octants can be stored optimally.
		std::vector<Octant> mChildren;

	};
}

#endif //RAYTRACER_OCTANT_HPP