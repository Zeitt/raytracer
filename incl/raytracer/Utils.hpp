#ifndef RAYTRACER_UTILS_HPP
#define RAYTRACER_UTILS_HPP

#include <glm.hpp>
#include <gtx/compatibility.hpp>

#include <random>

namespace raytracer
{
	/**
	 * @brief Namespace containing some useful colors as constants.
	 * Should match HTML/"Web" color naming.
	 */
	namespace colors
	{
		inline constexpr glm::vec3 gScarlet(){return glm::vec3{1.f, 0.14f, 0.f};}
		inline constexpr glm::vec3 gSkyBlue(){return glm::vec3{0.53f, 0.8f, 0.92f};}
		inline constexpr glm::vec3 gAzureWhite(){return glm::vec3{0.94f,1.f,1.f};}
		inline constexpr glm::vec3 gForest(){return glm::vec3{0.13f,0.545f,0.13f};}
		inline constexpr glm::vec3 gGold(){return glm::vec3{1.f,0.875f,0.f};}
		inline constexpr glm::vec3 gRoyalPurple(){ return glm::vec3{120.f/255.f, 81.f/255.f, 169.f/255.f};}
	}

	/**
	 * @brief Utility for generating random values.
	 */
	class Randomizer
	{
	public:
		/**
		 * @brief Constructor.
		 */
		explicit Randomizer();

		/**
		 * @brief Randomize single value.
		 * @return Random float between 0.f and 1.f
		 */
		float randomFloat();
		/**
		 * @brief Randomize 2-dimensional vector.
		 * @return Randomized vector with both values being between 0.f and 1.f
		 */
		glm::vec2 randomInPixel();
		/**
		 * @brief Randomize 3-dimensional vector.
		 * @return Randomized vector with all values being between 0.f and 1.f
		 */
		glm::vec3 randomInUnitSphere();

		/**
		 * @brief Perlin noise generation.
		 * @param point Coordinate used for sampling.
		 * @return Random float.
		 */
		inline float perlinNoise(glm::vec3 point) { return mPerlin.noise(point); }
		/**
		 * @brief Perlin turbulence.  Composite noise that has multiple summed frequencies.
		 * @param point Coordinate used for sampling.
		 * @return Random float.
		 */
		inline float perlinTurb(glm::vec3 point) { return mPerlin.turb(point); }
	private:
		std::mt19937 mGenerator{};
		std::uniform_real_distribution<float> mDistribution;

		/**
		 * @brief Internal utlity class for Randomizer::perlinNoise() & Randomizer::perlinTurb()
		 */
		class Perlin
		{
		public:
			void init(Randomizer* randomizer);
			float noise(glm::vec3) const;
			float turb(glm::vec3 p, size_t depth=7) const;
		private:
			std::vector<glm::vec3> mFloats{};
			std::vector<glm::tvec3<size_t>> mPerm{};
		};
		Perlin mPerlin{};
	};


}

#endif //RAYTRACER_UTILS_HPP
