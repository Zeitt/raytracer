#ifndef RAYTRACER_PLANES_HPP
#define RAYTRACER_PLANES_HPP

#include <raytracer/Ray.hpp>
#include <raytracer/AABB.hpp>
#include <raytracer/Materials.hpp>
#include <raytracer/Object.hpp>

#include <glm.hpp>

namespace raytracer
{
	/**
	 * @brief Planar vertical surface
	 */
	class PlaneXY final : public Object
	{
	public:
		/**
		 * @brief Can be used to construct planar object alongside XY axises.
		 * @param begin Starting point in [x,y]
		 * @param end End point in [x,y]
		 * @param z Position in Z direction.
		 * @param mat Material sampled if hit occurs.
		 */
		explicit PlaneXY(glm::vec2 begin, glm::vec2 end, float z, Material* mat);
		/**
		 * @copydoc Object::hitTest()
		 */
		bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;
		/**
		 * @copydoc Object::getAABB()
		 */
		inline AABB getAABB() const final
		{
			return AABB
			{
				// Magic 0.0001f = some depth for object to avoid collision problems
				glm::vec3{mOrigin, mK-0.0001f},
				glm::vec3{mEnd, mK+0.0001f},
			};
		}
	private:
		glm::vec2 mOrigin{};
		glm::vec2 mEnd{};
		float mK{};
	};


	/**
	 * @brief Planar horizontal surface
	 */
	class PlaneXZ final : public Object
	{
	public:
		/**
		 * @brief Can be used to construct planar object alongside XZ axises.
		 * @param begin Starting point in [x,z]
		 * @param end End point in [x,z]
		 * @param y Position in Y direction.
		 * @param mat Material sampled if hit occurs.
		 */
		explicit PlaneXZ(glm::vec2 begin, glm::vec2 end, float y, Material* mat);
		/**
		 * @copydoc Object::hitTest()
		 */
		bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;
		/**
		 * @copydoc Object::getAABB()
		 */
		inline AABB getAABB() const final
		{
			return AABB
			{
				// Magic 0.0001f = some depth for object to avoid collision problems
				glm::vec3{mOrigin.x, mK-0.0001f, mOrigin.y},
				glm::vec3{mEnd.x, mK+0.0001f, mEnd.y},
			};
		}

	private:
		glm::vec2 mOrigin{};
		glm::vec2 mEnd{};
		float mK{};
	};

	/**
	 * @brief Planar vertical surface
	 */
	class PlaneYZ final : public Object
	{
	public:
		/**
		 * @brief Can be used to construct planar object alongside YZ axises.
		 * @param begin Starting point in [y,z]
		 * @param end End point in [y,z]
		 * @param z Position in X direction.
		 * @param mat Material sampled if hit occurs.
		 */
		explicit PlaneYZ(glm::vec2 begin, glm::vec2 end, float x, Material* mat);
		/**
		 * @copydoc Object::hitTest()
		 */
		bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;
		/**
		 * @copydoc Object::getAABB()
		 */
		inline AABB getAABB() const final
		{
			return AABB
			{
				// Magic 0.0001f = some depth for object to avoid collision problems
				glm::vec3{mK-0.0001f, mOrigin},
				glm::vec3{mK+0.0001f, mEnd},
			};
		}
	private:
		glm::vec2 mOrigin{};
		glm::vec2 mEnd{};
		float mK{};
	};
}

#endif //RAYTRACER_PLANES_HPP
