#ifndef RAYTRACER_SPHERE_HPP
#define RAYTRACER_SPHERE_HPP

#include <raytracer/Object.hpp>

#include <glm.hpp>

namespace raytracer
{
	class Material;
	class Sphere final : public Object
	{
	public:
		/**
		 * @brief Constructors Sphere object.
		 * @param position Where origin will be.
		 * @param radius Size from origin.
		 * @param material Material sampled if hit occurs.
		 */
		explicit Sphere(glm::vec3 position, float radius, Material* material);

		/**
		 * @copydoc Object::hitTest()
		 */
		bool hitTest(const Ray& r, float minDistance, float maxDistance, HitRecord &recordOut) const final;

		/**
		 * @copydoc Object::getAABB()
		 */
		AABB getAABB() const final;
	private:
		glm::vec3 mPos{};
		float mRadius{0};

		glm::vec2 getUV(glm::vec3 p) const;
	};
}

#endif //RAYTRACER_SPHERE_HPP
