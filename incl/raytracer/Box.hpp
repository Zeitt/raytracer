#ifndef RAYTRACER_BOX_HPP
#define RAYTRACER_BOX_HPP

#include <raytracer/Planes.hpp>
#include <raytracer/ObjectTransforms.hpp>

namespace raytracer
{
/**
 * @brief Box-like object.
 * Three dimensional object looking like box. Derives from Object and cannot be inherited from.
 * Method implementations are at ProjectRoot/src/Box.cpp.
 */
class Box final : public Object
{
public:
	/**
	 * @brief Constructor for Box-like objects.
	 * @param begin Point from which box will begin.
	 * @param end Point where box will end.
	 * @param material Material used for sampling.
	 */
	explicit Box(glm::vec3 begin, glm::vec3 end, Material* material);
	/**
	 * @brief Default destructor.
	 */
	~Box() final = default;

	/**
	 * @copydoc Object::hitTest()
	 */
	bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;

	/**
	 * @copydoc Object::getAABB()
	 */
	inline AABB getAABB() const final { return mAABB; }

private:
	// Minimum and Maximum bounds
	AABB mAABB{};

	// Walls
	PlaneXY mBack;
	PlaneXY mFront;
	PlaneXZ mTop;
	PlaneXZ mBottom;
	PlaneYZ mLeft;
	PlaneYZ mRight;
	// These are actually used, but right now they require keeping non-flipped alive.
	Flipped mBackFlipped;
	Flipped mBottomFlipped;
	Flipped mRightFlipped;
	// Pointers to walls defined above for hitTest.
	std::vector<Object const *> mSides{};
};
}


#endif //RAYTRACER_BOX_HPP
