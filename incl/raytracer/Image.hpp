#ifndef RAYTRACER_IMAGE_HPP
#define RAYTRACER_IMAGE_HPP

#include <glm.hpp>

#include <vector>
#include <string>

namespace raytracer
{
/**
 * @brief Class for loading and storing three channel Images.
 */
class Image final
{
public:
	/**
	 * @brief Default constructor.
	 */
	explicit Image() = default;
	/**
	 * @brief Create RGB Image from width and height.
	 * @param width Width of image.
	 * @param height Height of image.
	 */
	explicit Image(size_t width, size_t height);
	/**
	 * @brief Load image from disk.
	 * Image is loaded with STB. Tested formats are HDR, PNG & JPG
	 * @param path Path to image file.
	 */
	explicit Image(const char* path);
	/**
	 * @brief Default destructor.
	 */
	~Image() = default;

	/**
	 * @brief Store pixel to Image
	 * @param x Position in x-axis. Has to be less than width.
	 * @param y Position in y-axis. Has to be less than height.
	 * @param value RGB value to be stored.
	 */
	void store(size_t x, size_t y, glm::vec3 value);
	/**
	 * @brief Load pixel from Image.
	 * @param x Position in x-axis. Has to be less than width.
	 * @param y Position in y-axis. Has to be less than height.
	 * @return RGB value in pixel.
	 */
	glm::vec3 load(size_t x, size_t y) const;

	/**
	 * @brief Saves Radiance HDR image to disk.
	 * STB-library is used to save images. Format is R32G32B32.
	 * @param path Where to save Image.
	 * @return True if saving image was successful. False otherwise.
	 */
	bool saveHDR(const std::string& path) const;
	/**
	 * @brief Saves PNG file ot disk.
	 * STB-library is used to save images. Format is R8G8B8.
	 * @param path Where to save Image.
	 * @return True if saving image was successful. False otherwise.
	 */
	bool savePNG(const std::string& path) const;

	/**
	 * @brief Getter for width of Image.
	 * @return Width of image.
	 */
	inline size_t getWidth() const { return mWidth; }
	/**
	 * @brief Getter for height of Image.
	 * @return Height of image.
	 */
	inline size_t getHeight() const { return mHeight; }
private:
	size_t mWidth{0};
	size_t mHeight{0};
	std::vector<glm::vec3> mData;

	// Used by savePNG to clamp pixels before saving.
	glm::tvec3<uint8_t> clamp(glm::vec3 col) const;
};
}

#endif //RAYTRACER_IMAGE_HPP
