#ifndef RAYTRACER_MATERIALS_HPP
#define RAYTRACER_MATERIALS_HPP

#include <raytracer/Utils.hpp>
#include <raytracer/Ray.hpp>
#include <raytracer/HitRecord.hpp>
#include <raytracer/Texture.hpp>

namespace raytracer
{
//TODO: Refactor after documentation is completed to use functors

/**
 * @brief Base class for Materials used for shading objects.
 */
class Material
{
public:
	/**
	 * @brief Default constructor.
	 */
	explicit Material() = default;
	/**
	 * @brief Default destructor.
	 */
	virtual ~Material() = default;
	//TODO: Rename function
	/**
	 * @brief Tries to scatter Ray. Outputs scattered Ray and attenuation if successful.
	 * @param ray Input ray.
	 * @param hitRecord Data passed between ray hits.
	 * @param attenuation If ray was scattered/absorbed then this contains attenuation.
	 * @param scattered Scattered ray.
	 * @return True if incoming Ray was scattered. False otherwise.
	 */
	virtual bool scatter(const Ray& ray, const HitRecord& hitRecord, glm::vec3& attenuation, Ray& scattered) const = 0;
	/**
	 * @brief How much material emits light. Overridden by LightMaterial.
	 * @param uv Texture coordinate for sampling.
	 * @param point Point coordinate for sampling.
	 * @return Emission.
	 */
	virtual glm::vec3 emitted(glm::vec2 uv, glm::vec3 point) const { return {}; }
protected:
	// True randomizer can not be constant, but other data in Materials doesnt need to change.
	mutable Randomizer mRandomizer{};
};

/**
 * @brief Diffuse/Matte material.
 */
class Lambertian final : public Material
{
public:
	/**
	 * @brief Constructor for Lambertian material.
	 * @param diffuse Texture used for sampling.
	 */
	explicit Lambertian(Texture* diffuse);
	/**
	 * @brief Default destructor.
	 */
	~Lambertian() final = default;

	/**
	 * @copydoc Material::scatter()
	 * @return true since this material type always scatters.
	 */
	bool scatter(const Ray& ray, const HitRecord& record, glm::vec3& attenuation, Ray& scattered) const final;
private:
	Texture* mDiffuse{nullptr};
};

/**
 * @brief Metallic material.
 */
class Metal final : public Material
{
public:
	/**
	 * @brief Constructor for metallic material.
	 * @param diffuse
	 * @param roughness
	 */
	explicit Metal(Texture* diffuse, float roughness);
	/**
	 * @brief Default destructor
	 */
	~Metal() final = default;
	/**
	 * @copydoc Material::scatter()
	 * @return true if Ray was scattered and attenuation is something.
	 */
	bool scatter(const Ray& ray, const HitRecord& record, glm::vec3& attenuation, Ray& scattered) const final;
private:
	Texture* mAlbedo{nullptr};
	float mRoughness;
};

/**
 * @brief Dielectric/Reflective material.
 */
class Dielectric final : public Material
{
public:
	/**
	 * @brief Constructor for Dielectric material.
	 * @param reflection Stregth of reflection when ray hits object.
	 */
	explicit Dielectric(float reflection);
	/**
	 * @brief Default destructor.
	 */
	~Dielectric() final = default;

	/**
	 * @copydoc Material::scatter()
	 * @return True since this material type always scatters.
	 */
	bool scatter(const Ray& ray, const HitRecord& record, glm::vec3& attenuation, Ray& scattered) const final;
private:
	float mReflection{0};
	glm::vec3 mColor{0.98f};

	bool refract(glm::vec3 v, glm::vec3 n, float ni, glm::vec3& refracted) const;
	float schlick(float cosine, float refl) const;
};

/**
 * @brief Material for Lights.
 */
class LightMaterial final : public Material
{
public:
	/**
	 * @brief Constructor for material used with Lights.
	 * @param val Color of Light. Should be over 1.0.
	 */
	explicit LightMaterial(glm::vec3 val):
		mEmit(val)
	{ }
	/**
	 * @brief Default destructor.
	 */
	~LightMaterial() final = default;

	/**
	 * @brief This material doesn't scatter rays. Here just to fulfill api. Does nothing.
	 * @return False
	 */
	bool scatter(const Ray&, const HitRecord &record, glm::vec3 &attenuation, Ray &scattered) const final
	{
		// This material doesnt scatter Rays.
		return false;
	}
	//TODO: Make use of uv and point or remove
	inline glm::vec3 emitted(glm::vec2, glm::vec3) const final
	{
		return mEmit;
	}
private:
	glm::vec3 mEmit{8.f};
};
}

#endif //RAYTRACER_MATERIALS_HPP
