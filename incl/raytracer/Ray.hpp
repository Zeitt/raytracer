#ifndef RAYTRACER_RAY_HPP
#define RAYTRACER_RAY_HPP

#include <glm.hpp>

namespace raytracer
{
/**
 * @brief Class containing functionality for rays used in raytracing.
 */
class Ray final
{
public:
	/**
	 * @brief Constructor for Ray.
	 * @param origin Where Ray starts.
	 * @param direction To what direction Ray is going.
	 */
	explicit Ray(glm::vec3 origin, glm::vec3 direction):
		mOrigin(origin),
		mDirection(direction)
	{}
	/**
	 * @brief Default destructor.
	 */
	~Ray() = default;

	/**
	 * @brief Point at certain distance.
	 * @param distance Distance from Origin going to Direction.
	 * @return point.
	 */
	inline glm::vec3 pointAt(float distance) const { return mOrigin + (distance * mDirection); }
	/**
	 * @brief Getter for origin of Ray.
	 * @return Origin of Ray.
	 */
	inline glm::vec3 getOrigin() const { return mOrigin; }
	/**
	 * @brief Getter for Ray direction.
	 * @return Direction of Ray.
	 */
	inline glm::vec3 getDirection() const { return mDirection; }
private:
	glm::vec3 mOrigin{};
	glm::vec3 mDirection{};
};
};

#endif //RAYTRACER_RAY_HPP
