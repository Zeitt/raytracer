#ifndef RAYTRACER_OBJECT_HPP
#define RAYTRACER_OBJECT_HPP

#include <raytracer/AABB.hpp>
#include <raytracer/Ray.hpp>
#include <raytracer/HitRecord.hpp>

namespace raytracer
{
// Predeclared to use within Object-class. See Material.hpp for details.
class Material;

/**
 * @brief Base class: All entities placed to World needs to implement this.
 */
class Object
{
public:
	/**
	 * @brief Default constructor.
	 */
	explicit Object() = default;
	/**
	 * @brief Default destructor.
	 */
	virtual ~Object() = default;
	/**
	 * @brief Most/all objects will have material.
	 * @param material Material sampled if hit occurs.
	 */
	explicit Object(Material* material):
		mMaterial(material)
	{}

	/**
	 * @brief Getter to AABB containing object bounds. Method is implemented by derived classes.
	 * @return Axis Aligned Bounding Box for Object.
	 */
	virtual AABB getAABB() const = 0;

	/**
	 * @brief Getter to Material bound to object.
	 * @return Pointer to constant Material bound to object.
	 */
	inline Material const* getMaterial() const { return mMaterial; }

	/**
	 * @brief Tests will ray hit current object.
	 * @param minDistance Closest distance to test.
	 * @param maxDistance Furthest distance to test.
	 * @param recordOut HitRecord of results
	 * @return True if hit occurred and recordOut was modified. Otherwise false.
	 */
	virtual bool hitTest(const Ray&, float minDistance, float maxDistance, HitRecord &recordOut) const = 0;

protected:
	Material* mMaterial{nullptr};
	AABB mAABB{};
};
}

#endif //RAYTRACER_OBJECT_HPP