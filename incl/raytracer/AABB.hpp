#ifndef RAYTRACER_AABB_HPP
#define RAYTRACER_AABB_HPP

#include <glm.hpp>

namespace raytracer
{
/**
 * @brief PoD-struct for Axis Aligned Bounding Box. Used in hit detection.
 */
struct AABB final
{
	glm::vec3 min{};
	glm::vec3 max{};
};

/**
 * @brief Tests does two Axis Aligned Bounding Boxes overlap.
 * @param first First Axis Aligned Bounding Box.
 * @param second Second Axis Aligned Bounding Box
 * @return True if boxes has any overlap. False if not.
 */
inline bool AABBsOverlap(const AABB& first, const AABB& second)
{
	const bool x = (first.max.x >= second.min.x) && (first.min.x <= second.max.x);
	const bool y = (first.max.y >= second.min.y) && (first.min.y <= second.max.y);
	const bool z = (first.max.z >= second.min.z) && (first.min.z <= second.max.z);
	return x && y && z;
}

}

#endif//RAYTRACER_AABB_HPP
