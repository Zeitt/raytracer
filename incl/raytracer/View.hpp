#ifndef RAYTRACER_VIEW_HPP
#define RAYTRACER_VIEW_HPP

#include <raytracer/Ray.hpp>
#include <raytracer/Image.hpp>
#include <raytracer/Utils.hpp>

#include <glm.hpp>

#include <functional>
#include <random>
#include <thread>
#include <atomic>
#include <stack>

namespace raytracer
{
	/**
	 * @brief Output of ray hit per pixel.
	 */
	struct TraceData
	{
		glm::vec3 color;
		glm::vec3 normal;
		float depth;
	};
	/**
	 * @brief Output tracing entire view.
	 */
	struct TraceOutput
	{
		Image color;
		Image normal;
		Image depth;
	};

	// Predeclared to avoid conflicts
	class World;
	/**
	 * @brief View to world.
	 */
	class View final
	{
	public:
		/**
		 * @brief Default constructor.
		 */
		View() = default;
		/**
		 * @brief Default copy constructor.
		 */
		View(View&) = default;
		/**
		 * @brief Default move constructor.
		 */
		View(View&&) = default;
		/**
		 * @brief Sets up View to World.
		 * @param from Origin of view.
		 * @param to Where view is looking at.
		 */
		void lookFromTo(glm::vec3 from, glm::vec3 to);

		/**
		 * @brief Traces entire World from View.
		 * @param task Shading functionality called when hit detection is done.
		 * @return Output of trace.
		 */
		TraceOutput trace(const std::function<TraceData(const Ray&, const World*, glm::tvec2<size_t> coord)>& task);
	private:
		float mVFov{};
		size_t mWidth{};
		size_t mHeight{};
		glm::vec3 mPos{0.f,0.f,0.f};

		glm::vec3 mHorizontal{};
		glm::vec3 mVertical{};
		float mDepth = -1.f;
		glm::vec3 mLowerLeft;

		std::vector<glm::vec2> mSampleTable;
		Randomizer mRand;

		size_t mThreadCount{1};

		struct Tile
		{
			glm::tvec2<size_t> begin;
			glm::tvec2<size_t> end;
		};
		std::vector<Tile> mTiles;

		World* mWorld{nullptr};

		/**
		 * @brief Casts ray from coordinate inside view.
		 */
		Ray rayCast(float u, float v);

		View(World*, float vFovDegree, size_t width, size_t height, size_t sampleCount, bool useThreading);
		friend class World;
	};
}

#endif //RAYTRACER_VIEW_HPP
