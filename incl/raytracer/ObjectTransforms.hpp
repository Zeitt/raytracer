#ifndef RAYTRACER_OBJECTTRANSFORMS_HPP
#define RAYTRACER_OBJECTTRANSFORMS_HPP

#include <raytracer/Object.hpp>

namespace raytracer
{
/**
 * @brief Object that has inverted normal direction.
 */
class Flipped final : public Object
{
public:
	/**
	 * @brief Creates object with inverted normal direction.
	 * @param obj Object which normal direction will be flipped when accessed trough this class.
	 */
	explicit Flipped(const Object& obj);
	/**
	 * @brief Default destructor.
	 */
	~Flipped() final;

	/**
	 * @copydoc Object::hitTest()
	 */
	inline bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;

	/**
	 * @copydoc Object::getAABB()
	 */
	inline AABB getAABB() const final;
private:
	const Object& mObj;
};

/**
 * @brief Object that is rotated along Y-axis
 */
class YRotated final : public Object
{
public:
	/**
	 * @brief Creates object with rotation along Y-axis.
	 * @param obj Object that will be rotated.
	 * @param degreeAngle Angle in degrees to determine rotation.
	 */
	explicit YRotated(const Object& obj, float degreeAngle);
	/**
	 * @brief Default destructor.
	 */
	~YRotated() final;

	/**
	 * @copydoc Object::hitTest()
	 */
	inline bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;

	/**
	 * @copydoc Object::getAABB()
	 */
	inline AABB getAABB() const final;

private:
	const Object& mObj;
	float mAngle{};
	AABB mAABB{};
	float mThetaSin{};
	float mThetaCos{};
};

/**
 * @brief Creates Object that has been moved
 */
class Translated final : public Object
{
public:
	/**
	 * @brief Creates Translated Object.
	 * @param obj Object to translate
	 * @param offset How much object should be moved.
	 */
	Translated(const Object& obj, glm::vec3 offset);

	/**
	 * @copydoc Object::hitTest()
	 */
	bool hitTest(const Ray& ray, float minDistance, float maxDistance, HitRecord &recordOut) const final;

	/**
	 * @copydoc Object::getAABB()
	 */
	inline AABB getAABB() const final;
private:
	glm::vec3 mOffset{};
	const Object& mObj;
};
}


#endif //RAYTRACER_OBJECTTRANSFORMS_HPP
