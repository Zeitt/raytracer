# Raytracer

Multi-threaded C++ ray tracer made to complete two first books of ["Ray Tracing in One Weekend"](https://raytracing.github.io).

Not a production project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine testing purposes. 

**Important!** Only tested on Linux, to get working on Windows [CMakeLists.txt](./CMakeLists.txt) needs to be modified.

### Prerequisites & Installing

This program only needs CMake (3.14 or newer), C++14 capable compiler and matching std-library. 

```
#Installing on Arch linux
sudo pacman -Sy base-devel cmake
```

## Building

First generate project files with CMake (for example to build folder).

`cmake . -B build -DCMAKE_BUILD_TYPE=Release` 

Then CMake can be used to build generated files.

` cmake --build build --config Release`

And finally it can be ran.

`./build/raytracer`

### Documentation

Documentation can be generated with Doxygen.

`doxygen ./Doxyfile`

## Examples

This project has currently two scenes. Basic Cornell's box (default) and "Ball World".

You can switch with scenes by adding argument `BallWorld` or `CornellsBox` after executable.

`./raytracer BallWorld`

### Cornell's Box

Can be rendered with `CornellsBox` command line argument.

![cornell](./cornell.png)

### Ball World

Can be rendered with `BallWorld` command line argument.

![balls](./balls.png)

## Built With

- [CMake](https://cmake.org) - Build configuration generator.
- [Doxygen](https://www.doxygen.nl/index.html) - Documentation generator.

## License

This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details

## Acknowledgements

- Peter Shirley for [*Ray Tracing in One Weekend*](https://raytracing.github.io) book series.
- Billie Thompson for "[README-Template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)"
- TopTal for [gitignore.io](https://www.toptal.com/developers/gitignore)

 
